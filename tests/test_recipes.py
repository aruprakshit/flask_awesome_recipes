from flask import url_for
import pytest


def test_fetch_recipes(client, create_recipe_record, create_user_record):
    recipe1 = create_recipe_record(
        {
            "cook": "9 min",
            "inactive": "20 min",
            "level": "Easy",
            "prep": "5 min",
            "title": "Grilled Salmon I",
            "total": "34 min",
            "yields": "6 servings",
        }
    )

    recipe2 = create_recipe_record(
        {
            "cook": "45 min",
            "inactive": "",
            "level": "",
            "prep": "30 min",
            "title": "Grilled Salmon II",
            "total": "1 hr 15 min",
            "yields": "4 servings",
        }
    )
    user = create_user_record({"username": "john", "password": "cat01"})

    response = client.get(
        url_for("recipes.recipes_api", _method="GET"),
        headers=[("Authorization", "Bearer {}".format(user.access_token))],
    )

    payload = response.json
    assert len(payload) == 2
    assert "Grilled Salmon I" in list(map(lambda r: r["title"], payload))
    assert "Grilled Salmon II" in list(map(lambda r: r["title"], payload))


def test_update_a_recipe(client, create_recipe_record, create_user_record):
    recipe1 = create_recipe_record(
        {
            "cook": "9 min",
            "inactive": "20 min",
            "level": "Easy",
            "prep": "5 min",
            "title": "Grilled Salmon I",
            "total": "34 min",
            "yields": "6 servings",
        }
    )
    user = create_user_record({"username": "john", "password": "cat01"})
    response = client.put(
        url_for("recipes.recipes_api", _method="PUT", id=recipe1.id),
        json={"inactive": "10 min", "level": "easy"},
        headers=[("Authorization", "Bearer {}".format(user.access_token))],
    )
    payload = response.json
    assert response.status_code == 200
    assert payload["title"] == "Grilled Salmon I"
    assert payload["inactive"] == "10 min"
    assert payload["level"] == "easy"


def test_delete_a_recipe(client, create_recipe_record, create_user_record):
    recipe1 = create_recipe_record(
        {
            "cook": "9 min",
            "inactive": "20 min",
            "level": "Easy",
            "prep": "5 min",
            "title": "Grilled Salmon I",
            "total": "34 min",
            "yields": "6 servings",
        }
    )

    recipe2 = create_recipe_record(
        {
            "cook": "45 min",
            "inactive": "",
            "level": "",
            "prep": "30 min",
            "title": "Grilled Salmon II",
            "total": "1 hr 15 min",
            "yields": "4 servings",
        }
    )

    user = create_user_record({"username": "john", "password": "cat01"})

    response = client.delete(
        url_for("recipes.recipes_api", _method="DELETE", id=recipe1.id),
        headers=[("Authorization", "Bearer {}".format(user.access_token))],
    )
    payload = response.json
    assert response.status_code == 200
    assert payload["id"] == recipe1.id

    response = client.get(
        "/api/recipes/",
        headers=[("Authorization", "Bearer {}".format(user.access_token))],
    )
    payload = response.json
    assert len(payload) == 1


def test_create_a_recipe(client, create_user_record):
    user = create_user_record({"username": "john", "password": "cat01"})
    response = client.post(
        url_for("recipes.recipes_api", _method="POST"),
        json={
            "cook": "45 min",
            "inactive": "",
            "level": "",
            "prep": "30 min",
            "title": "Test Recipe",
            "total": "1 hr 15 min",
            "yields": "3 servings",
        },
        headers=[("Authorization", "Bearer {}".format(user.access_token))],
    )
    payload = response.json
    assert payload["title"] == "Test Recipe"
    assert payload["cook"] == "45 min"
    assert payload["prep"] == "30 min"
    assert payload["yields"] == "3 servings"
    assert payload["total"] == "1 hr 15 min"
    assert response.status_code == 201

    response = client.get(
        url_for("recipes.recipes_api", _method="GET"),
        headers=[("Authorization", "Bearer {}".format(user.access_token))],
    )
    payload = response.json
    assert len(payload) == 1


def test_fetch_a_recipe(client, create_recipe_record, create_user_record):
    recipe1 = create_recipe_record(
        {
            "cook": "9 min",
            "inactive": "20 min",
            "level": "Easy",
            "prep": "5 min",
            "title": "Grilled Salmon I",
            "total": "34 min",
            "yields": "6 servings",
        }
    )

    recipe2 = create_recipe_record(
        {
            "cook": "45 min",
            "inactive": "",
            "level": "",
            "prep": "30 min",
            "title": "Grilled Salmon II",
            "total": "1 hr 15 min",
            "yields": "4 servings",
        }
    )

    user = create_user_record({"username": "john", "password": "cat01"})
    response = client.get(
        url_for("recipes.recipes_api", _method="GET", id=recipe2.id),
        headers=[("Authorization", "Bearer {}".format(user.access_token))],
    )
    payload = response.json
    assert response.status_code == 200
    assert payload["id"] == recipe2.id
    assert payload["title"] == "Grilled Salmon II"
    assert payload["cook"] == "45 min"
    assert payload["prep"] == "30 min"
    assert payload["yields"] == "4 servings"
    assert payload["total"] == "1 hr 15 min"
    assert not payload["inactive"]
    assert not payload["level"]


def test_attach_a_category_to_a_recipe(
    client, create_recipe_record, create_category_record, create_user_record
):
    recipe1 = create_recipe_record(
        {
            "cook": "45 min",
            "inactive": "",
            "level": "",
            "prep": "30 min",
            "title": "Grilled Salmon II",
            "total": "1 hr 15 min",
            "yields": "4 servings",
        }
    )

    category1 = create_category_record({"title": "Grilled Chicken"})
    user = create_user_record({"username": "john", "password": "cat01"})

    response = client.post(
        url_for(
            "recipe_categories.recipe_categories_api",
            _method="POST",
            recipe_id=recipe1.id,
        ),
        json={"id": category1.id},
        headers=[("Authorization", "Bearer {}".format(user.access_token))],
    )

    payload = response.json
    assert response.status_code == 201
    assert isinstance(payload, list)
    assert len(payload) == 1
    assert {"id": category1.id, "title": "Grilled Chicken"} in payload


def test_remove_a_category_from_a_recipe(
    client, create_recipe_record, create_category_record, create_user_record
):
    recipe1 = create_recipe_record(
        {
            "cook": "45 min",
            "inactive": "",
            "level": "",
            "prep": "30 min",
            "title": "Grilled Salmon II",
            "total": "1 hr 15 min",
            "yields": "4 servings",
        }
    )

    category1 = create_category_record({"title": "Grilled Chicken"})
    category2 = create_category_record({"title": "Grilled Chicken"})
    recipe1.add_category(category1.id)
    recipe1.add_category(category2.id)
    user = create_user_record({"username": "john", "password": "cat01"})

    response = client.delete(
        url_for(
            "recipe_categories.recipe_categories_api",
            _method="DELETE",
            recipe_id=recipe1.id,
            id=category2.id,
        ),
        headers=[("Authorization", "Bearer {}".format(user.access_token))],
    )

    assert response.status_code == 204
    assert len(recipe1.categories) == 1


@pytest.mark.usefixtures("client_class", "recipe", "category")
class TestUnauthorizedRecipesAccess:
    def test_fetch_recipes(self):
        assert (
            self.client.get(
                url_for("recipes.recipes_api", _method="GET")
            ).status_code
            == 401
        )

    def test_update_a_recipe(self):
        assert (
            self.client.put(
                url_for(
                    "recipes.recipes_api", _method="PUT", id=self.recipe.id
                ),
                json={"inactive": "10 min", "level": "easy"},
            ).status_code
            == 401
        )

    def test_delete_a_recipe(self):
        assert (
            self.client.delete(
                url_for(
                    "recipes.recipes_api", _method="DELETE", id=self.recipe.id
                )
            ).status_code
            == 401
        )

    def test_create_a_recipe(self):
        assert (
            self.client.post(
                url_for("recipes.recipes_api", _method="POST"),
                json={"cook": "45 min", "inactive": ""},
            ).status_code
            == 401
        )

    def test_fetch_a_recipe(self):
        assert (
            self.client.get(
                url_for("recipes.recipes_api", _method="GET", id=self.recipe.id)
            ).status_code
            == 401
        )

    def test_attach_a_category_to_a_recipe(self):
        assert (
            self.client.post(
                url_for(
                    "recipe_categories.recipe_categories_api",
                    _method="POST",
                    recipe_id=self.recipe.id,
                ),
                json={"id": self.category.id},
            ).status_code
            == 401
        )

    def test_remove_a_category_from_a_recipe(self):
        assert (
            self.client.delete(
                url_for(
                    "recipe_categories.recipe_categories_api",
                    _method="DELETE",
                    recipe_id=self.recipe.id,
                    id=self.category.id,
                )
            ).status_code
            == 401
        )

