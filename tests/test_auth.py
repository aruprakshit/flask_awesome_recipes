from flask import url_for
import pytest


def test_signup(client):
    response = client.post(
        url_for("auth.auth_signup_api", _method="POST"),
        json={"username": "test01", "password": "cat01"},
    )

    assert response.status_code == 201
    assert isinstance(response.data, bytes)
    assert str(response.data, "utf-8") == "An account is crated for {}".format(
        "test01"
    )


def test_login(client, create_user_record):
    user = create_user_record({"username": "test01", "password": "cat01"})

    response = client.post(
        url_for("auth.auth_login_api", _method="POST"),
        json={"username": user.username, "password": "cat01"},
    )

    payload = response.json

    assert response.status_code == 200
    assert isinstance(payload, dict)
    assert "access_token" in payload
    assert "expires_in" in payload
    assert "expires_on" in payload
    assert "refresh_token" in payload


def test_logout(client, create_user_record):
    user = create_user_record({"username": "john", "password": "cat01"})
    response = client.delete(
        "/auth/logout", headers=[("Authorization", "Bearer {}".format(""))]
    )

    assert response.status_code == 401

    response = client.delete(
        "/auth/logout",
        headers=[("Authorization", "Bearer {}".format(user.access_token))],
    )

    assert response.status_code == 204

    assert (
        client.get(
            url_for("categories.categories_api", _method="GET"),
            headers=[("Authorization", "Bearer {}".format(user.access_token))],
        ).status_code
        == 401
    )
