import pytest

from app import create_app
from app.db import init_db
from app.models.recipe import Recipe
from app.models.category import Category
from app.models.user import User


@pytest.fixture
def app():
    """Create and configure a new app instance for each test."""
    # create the app with common test config
    app = create_app({"TESTING": True, "DATABASE": "awesome_recipes_test"})

    result = app.test_cli_runner().invoke(args=["db", "prepare_for_test"])
    # print(result.output)

    return app


@pytest.fixture
def create_user_record():
    created_records = []

    def _create_recipe_record(attrs):
        record = User.create(**attrs)
        record.set_tokens()
        created_records.append(record)
        return record

    yield _create_recipe_record

    for record in created_records:
        User.destroy(record.id)


@pytest.fixture
def create_recipe_record():

    created_records = []

    def _create_recipe_record(attrs):
        record = Recipe.create(**attrs)
        created_records.append(record)
        return record

    yield _create_recipe_record

    for record in created_records:
        Recipe.destroy(record.id)


@pytest.fixture
def create_category_record():

    created_records = []

    def _create_category_record(attrs):
        record = Category.create(**attrs)
        created_records.append(record)
        return record

    yield _create_category_record

    for record in created_records:
        Category.destroy(record.id)


@pytest.fixture
def category(request, create_category_record):
    attrs = {"title": "Grilled Chicken"}
    if request.cls is not None:
        request.cls.category = create_category_record(attrs)
    else:
        return create_category_record(attrs)


@pytest.fixture
def recipe(request, create_recipe_record):
    attrs = {
        "cook": "45 min",
        "inactive": "",
        "level": "",
        "prep": "30 min",
        "title": "Grilled Salmon II",
        "total": "1 hr 15 min",
        "yields": "4 servings",
    }
    if request.cls is not None:
        request.cls.recipe = create_recipe_record(attrs)
    else:
        return create_category_record(attrs)
