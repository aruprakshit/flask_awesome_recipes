from flask import url_for

import pytest


def test_fetch_categories(client, create_category_record, create_user_record):
    category1 = create_category_record({"title": "Grilled Chicken"})
    category2 = create_category_record({"title": "Chicken"})
    category3 = create_category_record({"title": "Grilling"})
    user = create_user_record({"username": "john", "password": "cat01"})
    response = client.get(
        url_for("categories.categories_api", _method="GET"),
        headers=[("Authorization", "Bearer {}".format(user.access_token))],
    )
    payload = response.json
    assert response.status_code == 200
    assert isinstance(payload, list)
    assert len(payload) == 3
    assert {"title": category1.title, "id": category1.id} in payload
    assert {"title": category2.title, "id": category2.id} in payload
    assert {"title": category3.title, "id": category3.id} in payload


def test_create_category(client, create_user_record):
    user = create_user_record({"username": "john", "password": "cat01"})
    response = client.post(
        url_for("categories.categories_api", _method="POST"),
        json={"title": "Grilling"},
        headers=[("Authorization", "Bearer {}".format(user.access_token))],
    )
    payload = response.json

    assert response.status_code == 201
    assert isinstance(payload, dict)
    assert payload["title"] == "Grilling"
    assert "id" in payload


def test_delete_a_category(client, create_category_record, create_user_record):
    category1 = create_category_record({"title": "Grilled Chicken"})
    category2 = create_category_record({"title": "Grilling"})
    user = create_user_record({"username": "john", "password": "cat01"})

    response = client.delete(
        url_for("categories.categories_api", _method="DELETE", id=category1.id),
        headers=[("Authorization", "Bearer {}".format(user.access_token))],
    )
    payload = str(response.data, "utf-8")
    assert response.status_code == 204
    assert isinstance(payload, str)
    assert not payload

    response = client.get(
        url_for("categories.categories_api", _method="GET"),
        headers=[("Authorization", "Bearer {}".format(user.access_token))],
    )
    payload = response.json
    assert response.status_code == 200
    assert isinstance(payload, list)
    assert len(payload) == 1
    assert {"title": category1.title, "id": category1.id} not in payload
    assert {"title": category2.title, "id": category2.id} in payload


def test_update_a_category(client, create_category_record, create_user_record):
    category = create_category_record({"title": "Grilled Chicken"})
    user = create_user_record({"username": "john", "password": "cat01"})

    response = client.put(
        url_for("categories.categories_api", _method="PUT", id=category.id),
        json={"title": "Grilled Mutton"},
        headers=[("Authorization", "Bearer {}".format(user.access_token))],
    )

    payload = response.json

    assert response.status_code == 200
    assert isinstance(payload, dict)
    assert payload["title"] == "Grilled Mutton"
    assert payload["id"] == category.id


@pytest.mark.usefixtures("client_class", "category")
class TestUnauthorizedCategoriesAccess:
    def test_fetch_categories(self):
        assert (
            self.client.get(
                url_for("categories.categories_api", _method="GET")
            ).status_code
            == 401
        )

    def test_update_a_category(self):
        assert (
            self.client.put(
                url_for(
                    "categories.categories_api",
                    _method="PUT",
                    id=self.category.id,
                ),
                json={"title": "Grilled Mutton"},
            ).status_code
            == 401
        )
