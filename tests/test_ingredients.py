from flask import url_for
import pytest


def test_update_ingredients(client, create_recipe_record, create_user_record):
    user = create_user_record({"username": "john", "password": "cat01"})
    recipe1 = create_recipe_record(
        {
            "cook": "9 min",
            "inactive": "20 min",
            "level": "Easy",
            "prep": "5 min",
            "title": "Grilled Salmon I",
            "total": "34 min",
            "yields": "6 servings",
        }
    )

    ingredients = [
        "One 1-inch-thick slice pineapple, peeled and cored",
        "One 8- to 12-ounce wheel Brie or Camembert, cut in half horizontally",
        "8 slices prosciutto (about 8 ounces)",
    ]

    recipe1.create_ingredients(ingredients)

    response = client.put(
        url_for(
            "ingredients.ingredients_api",
            _method="PUT",
            recipe_id=recipe1.id,
            position=1,
        ),
        data="1 cup all-purpose flour",
        headers=[("Authorization", "Bearer {}".format(user.access_token))],
    )

    payload = response.json
    assert response.status_code == 200
    assert isinstance(payload, list)
    assert ingredients[0] in payload
    assert ingredients[1] not in payload
    assert ingredients[2] in payload
    assert len(payload) == 3
    assert "1 cup all-purpose flour" in payload


def test_delete_ingredients(client, create_recipe_record, create_user_record):
    user = create_user_record({"username": "john", "password": "cat01"})
    recipe1 = create_recipe_record(
        {
            "cook": "9 min",
            "inactive": "20 min",
            "level": "Easy",
            "prep": "5 min",
            "title": "Grilled Salmon I",
            "total": "34 min",
            "yields": "6 servings",
        }
    )

    ingredients = [
        "One 1-inch-thick slice pineapple, peeled and cored",
        "One 8- to 12-ounce wheel Brie or Camembert, cut in half horizontally",
        "8 slices prosciutto (about 8 ounces)",
    ]

    recipe1.create_ingredients(ingredients)
    response = client.delete(
        url_for(
            "ingredients.ingredients_api",
            _method="DELETE",
            recipe_id=recipe1.id,
        ),
        data="8 slices prosciutto (about 8 ounces)",
        headers=[("Authorization", "Bearer {}".format(user.access_token))],
    )

    payload = response.json

    assert response.status_code == 200
    assert isinstance(payload, list)
    assert ingredients[0] in payload
    assert ingredients[1] in payload
    assert ingredients[2] not in payload
    assert len(payload) == 2


def test_add_ingredients(client, create_recipe_record, create_user_record):
    user = create_user_record({"username": "john", "password": "cat01"})
    recipe1 = create_recipe_record(
        {
            "cook": "9 min",
            "inactive": "20 min",
            "level": "Easy",
            "prep": "5 min",
            "title": "Grilled Salmon I",
            "total": "34 min",
            "yields": "6 servings",
        }
    )

    ingredients = [
        "One 1-inch-thick slice pineapple, peeled and cored",
        "One 8- to 12-ounce wheel Brie or Camembert, cut in half horizontally",
        "8 slices prosciutto (about 8 ounces)",
        "Crackers and bread, for serving",
    ]

    response = client.post(
        url_for(
            "ingredients.ingredients_api", _method="POST", recipe_id=recipe1.id
        ),
        json=ingredients,
        headers=[("Authorization", "Bearer {}".format(user.access_token))],
    )

    payload = response.json

    assert response.status_code == 201
    assert isinstance(payload, list)
    assert ingredients[0] in payload
    assert ingredients[1] in payload
    assert ingredients[2] in payload


def test_fetch_recipes(client, create_recipe_record, create_user_record):
    user = create_user_record({"username": "john", "password": "cat01"})

    recipe1 = create_recipe_record(
        {
            "cook": "9 min",
            "inactive": "20 min",
            "level": "Easy",
            "prep": "5 min",
            "title": "Grilled Salmon I",
            "total": "34 min",
            "yields": "6 servings",
        }
    )

    ingredients = [
        "One 1-inch-thick slice pineapple, peeled and cored",
        "One 8- to 12-ounce wheel Brie or Camembert, cut in half horizontally",
        "8 slices prosciutto (about 8 ounces)",
    ]

    recipe1.create_ingredients(ingredients)

    response = client.get(
        url_for(
            "ingredients.ingredients_api", _method="GET", recipe_id=recipe1.id
        ),
        headers=[("Authorization", "Bearer {}".format(user.access_token))],
    )

    payload = response.json

    assert response.status_code == 200
    assert isinstance(payload, list)
    assert ingredients[0] in payload
    assert ingredients[1] in payload
    assert ingredients[2] in payload
