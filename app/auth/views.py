from flask import Blueprint, request, jsonify, g
from flask.views import MethodView
from http import HTTPStatus
from ..models.user import User
from flask_httpauth import HTTPTokenAuth


bp = Blueprint("auth", __name__, url_prefix="/auth")
auth = HTTPTokenAuth()


@auth.verify_token
def verify_token(token):
    g.current_user = User.find_by("access_token", token)
    return g.current_user is not None


class AuthSignUpApi(MethodView):
    """Auth signup resource"""

    def post(self):
        user = User.create(**request.json)
        return (
            "An account is crated for {}".format(user.username),
            HTTPStatus.CREATED,
        )


class AuthLoginApi(MethodView):
    """Auth login resource"""

    def post(self):
        user = User.find_by("username", request.json["username"])
        if user.check_password(request.json["password"]):
            user.set_tokens()
            return jsonify(user.to_dict()), HTTPStatus.OK


class AuthLogoutApi(MethodView):
    """Auth logout resource"""

    def delete(self):
        g.current_user.reset_token()
        return "", HTTPStatus.NO_CONTENT


# define the API resources
auth_signup_view = AuthSignUpApi.as_view("auth_signup_api")
auth_login_view = AuthLoginApi.as_view("auth_login_api")
auth_logout_view = auth.login_required(AuthLogoutApi.as_view("auth_logout_api"))

# add rules for api endpoints
bp.add_url_rule("/sign-up", view_func=auth_signup_view, methods=["POST"])
bp.add_url_rule("/login", view_func=auth_login_view, methods=["POST"])
bp.add_url_rule("/logout", view_func=auth_logout_view, methods=["DELETE"])

