from flask import Blueprint, request, jsonify, g
from flask.views import MethodView
from ..models.recipe import Recipe
from app.models.user import User
from flask_httpauth import HTTPTokenAuth
from http import HTTPStatus


bp = Blueprint("recipes", __name__, url_prefix="/api")
auth = HTTPTokenAuth()


@auth.verify_token
def verify_token(token):
    g.current_user = User.find_by("access_token", token)
    return g.current_user is not None


class RecipesApi(MethodView):
    """Recipes resource"""

    def get(self, id):
        if id is None:
            # return a list of recipes
            return (
                jsonify([row.to_dict() for row in Recipe.all()]),
                HTTPStatus.OK,
            )
        else:
            return jsonify(Recipe.find(id).to_dict()), HTTPStatus.OK

    def post(self):
        # create a new recipe
        recipe = Recipe.create(**request.json)
        return jsonify(recipe.to_dict()), HTTPStatus.CREATED

    def delete(self, id):
        # delete a single recipe
        Recipe.destroy(id)
        return jsonify(dict(id=id)), HTTPStatus.OK

    def put(self, id):
        # update a single recipe
        recipe = Recipe.find(id).update(**request.json)
        return jsonify(recipe.to_dict()), HTTPStatus.OK


# define the API resources
recipes_view = auth.login_required(RecipesApi.as_view("recipes_api"))

# add rules for api endpoints
bp.add_url_rule(
    "/recipes/", defaults={"id": None}, view_func=recipes_view, methods=["GET"]
)
bp.add_url_rule("/recipes/", view_func=recipes_view, methods=["POST"])
bp.add_url_rule(
    "/recipes/<string:id>",
    view_func=recipes_view,
    methods=["GET", "PUT", "DELETE"],
)

