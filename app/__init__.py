from flask import Flask, jsonify
import os
import pprint


def create_app(test_config=None):
    """Create and configure an instance of the Flask application."""
    app = Flask(__name__, instance_relative_config=True)

    app.config.from_mapping(
        # a default secret that should be overridden by instance config
        SECRET_KEY="dev",
        # store the database in the instance folder
        DATABASE="awesome_recipes_dev",
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile("config.py", silent=True)
    else:
        # load the test config if passed in
        app.config.update(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from app import db

    db.init_app(app)

    from .models import bcrypt

    bcrypt.init_app(app)

    @app.shell_context_processor
    def shell_context():
        from app.models.recipe import Recipe
        from app.models.user import User

        return {
            "app": app,
            "db": db.get_db(),
            "pp": pprint.PrettyPrinter(indent=4).pprint,
            "Recipe": Recipe,
            "User": User,
        }

    @app.route("/")
    def index():
        return jsonify({"hello": "World"})

    from .recipes.views import bp as recipes_blueprint

    app.register_blueprint(recipes_blueprint)

    from .ingredients.views import bp as ingredients_blueprint

    app.register_blueprint(ingredients_blueprint)

    from .categories.views import bp as categories_blueprint

    app.register_blueprint(categories_blueprint)

    from .recipe_categories.views import bp as recipe_categories_blueprint

    app.register_blueprint(recipe_categories_blueprint)

    from .auth.views import bp as auth_blueprint

    app.register_blueprint(auth_blueprint)

    return app


if __name__ == "__main__":
    import os

    if "WINGDB_ACTIVE" in os.environ:
        app.debug = False
    create_app().run(use_reloader=True)
