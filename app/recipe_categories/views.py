from flask import Blueprint, request, jsonify, g
from flask.views import MethodView
from ..models.recipe import Recipe
from app.models.user import User
from flask_httpauth import HTTPTokenAuth
from http import HTTPStatus

bp = Blueprint(
    "recipe_categories", __name__, url_prefix="/api/recipes/<string:recipe_id>"
)
auth = HTTPTokenAuth()


@auth.verify_token
def verify_token(token):
    g.current_user = User.find_by("access_token", token)
    return g.current_user is not None


class RecipeCategoriesApi(MethodView):
    """Recipe categories resource"""

    def post(self, recipe_id):
        """tags a recipe"""
        result = Recipe.find(recipe_id).add_category(request.json["id"])
        return (
            jsonify([*map(lambda item: item.to_dict(), result)]),
            HTTPStatus.CREATED,
        )

    def delete(self, recipe_id, id):
        """remove tags from recipe"""
        result = Recipe.find(recipe_id).remove_category(id)
        return "", HTTPStatus.NO_CONTENT


# define the API resources
recipe_categories_view = auth.login_required(
    RecipeCategoriesApi.as_view("recipe_categories_api")
)

# add rules for api endpoints
bp.add_url_rule(
    "/categories/", view_func=recipe_categories_view, methods=["POST"]
)
bp.add_url_rule(
    "/categories/<string:id>",
    view_func=recipe_categories_view,
    methods=["DELETE"],
)

