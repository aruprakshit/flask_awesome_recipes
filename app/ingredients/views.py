from flask import Blueprint, request, jsonify, g
from flask.views import MethodView
from app.models.recipe import Recipe
from app.models.user import User
from flask_httpauth import HTTPTokenAuth
from http import HTTPStatus


bp = Blueprint(
    "ingredients", __name__, url_prefix="/api/recipes/<string:recipe_id>"
)
auth = HTTPTokenAuth()


@auth.verify_token
def verify_token(token):
    g.current_user = User.find_by("access_token", token)
    return g.current_user is not None


class IngredientsApi(MethodView):
    """Ingredients resource"""

    def delete(self, recipe_id):
        # delete an ingredient
        name = request.data.decode("utf-8")
        result = Recipe.find(recipe_id).delete_ingredients(name)
        return jsonify(result), HTTPStatus.OK

    def put(self, recipe_id, position):
        # update an ingredient
        name = request.data.decode("utf-8")
        result = Recipe.find(recipe_id).update_ingredients(name, position)
        return jsonify(result), HTTPStatus.OK

    def post(self, recipe_id):
        # create a new ingredient(s)
        result = Recipe.find(recipe_id).create_ingredients(request.json)
        return jsonify(list(map(lambda r: r.name, result))), HTTPStatus.CREATED

    def get(self, recipe_id):
        result = Recipe.find(recipe_id).ingredients
        return jsonify(result), HTTPStatus.OK


# define the API resources
ingredients_view = auth.login_required(
    IngredientsApi.as_view("ingredients_api")
)

# add rules for api endpoints
bp.add_url_rule(
    "/ingredients/",
    view_func=ingredients_view,
    methods=["POST", "DELETE", "GET"],
)
bp.add_url_rule(
    "/ingredients/<int:position>", view_func=ingredients_view, methods=["PUT"]
)
