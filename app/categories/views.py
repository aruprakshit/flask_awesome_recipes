from flask import Blueprint, request, jsonify, g
from flask.views import MethodView
from app.models.category import Category
from app.models.user import User
from flask_httpauth import HTTPTokenAuth
from http import HTTPStatus


bp = Blueprint("categories", __name__, url_prefix="/api")
auth = HTTPTokenAuth()


@auth.verify_token
def verify_token(token):
    g.current_user = User.find_by("access_token", token)
    return g.current_user is not None


class CategoriesApi(MethodView):
    """Categories resource"""

    def get(self):
        return jsonify([row.to_dict() for row in Category.all()]), HTTPStatus.OK

    def post(self):
        result = Category.create(title=request.json["title"])
        return jsonify(result.to_dict()), HTTPStatus.CREATED

    def delete(self, id):
        Category.destroy(id)
        return "", HTTPStatus.NO_CONTENT

    def put(self, id):
        result = Category.update(id, title=request.json["title"])
        return jsonify(result.to_dict()), HTTPStatus.OK


# define the API resources
categories_view = auth.login_required(CategoriesApi.as_view("categories_api"))

# add rules for api endpoints
bp.add_url_rule(
    "/categories/", view_func=categories_view, methods=["GET", "POST"]
)

bp.add_url_rule(
    "/categories/<string:id>",
    view_func=categories_view,
    methods=["DELETE", "PUT"],
)

