from rethinkdb import RethinkDB
from collections import namedtuple
from rethinkdb.errors import ReqlRuntimeError

import click
import json
import pytest

from flask import current_app
from flask import g
from flask.cli import AppGroup, with_appcontext

db_cli = AppGroup("db")

Db = namedtuple("Db", "conn r")


def get_db():
    """Connect to the application's configured database. The connection
    is unique for each request and will be reused if this is called
    again.
    """
    r = RethinkDB()

    if "db" not in g:
        conn = r.connect(
            db=current_app.config["DATABASE"], host="localhost", port="28015"
        )
        g.db = Db(conn=conn, r=r)

    return g.db


def close_db(e=None):
    """If this request connected to the database, close the
    connection.
    """
    db = g.pop("db", None)

    if db is not None:
        db.conn.close()


def init_db():
    """Clear existing data and create new tables."""
    db = get_db()
    try:
        db.r.db_drop(current_app.config["DATABASE"]).run(db.conn)
    except ReqlRuntimeError as e:
        print(e)
    finally:
        db.r.db_create(current_app.config["DATABASE"]).run(db.conn)


@db_cli.command("init")
def init_db_command():
    """Clear existing data and create new tables."""
    init_db()
    click.echo("Initialized the database.")


@db_cli.command("seed")
def seed_db_command():
    """Load initial data into tables."""
    from .models.recipe import Recipe

    with current_app.open_resource("seed.json") as f:
        records = json.load(f)
        for record in records:
            Recipe.create(**record)

    click.echo("seeded the database.")


@db_cli.command("create_table")
@click.argument("name")
def create_table_command(name):
    db = get_db()
    db.r.table_create(name).run(db.conn)


@db_cli.command("prepare_for_test")
@click.pass_context
def prepare_for_test_command(ctx):
    ctx.forward(init_db_command)
    ctx.invoke(create_table_command, name="recipes")
    ctx.invoke(create_table_command, name="categories")
    ctx.invoke(create_table_command, name="users")
    click.echo("Reset the database.")


@click.command("test")
@click.option("--capture", is_flag=True)
@with_appcontext
def test_runner(capture):
    if capture:
        pytest.main(["-x", "tests", "--capture", "no"])
    else:
        pytest.main(["-x", "tests"])


def init_app(app):
    """Register database functions with the Flask app. This is called by
    the application factory.
    """
    app.teardown_appcontext(close_db)
    app.cli.add_command(db_cli)
    app.cli.add_command(test_runner)
