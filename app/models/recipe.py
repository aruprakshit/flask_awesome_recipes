from typing import List

from .base_model import BaseModel
from .category import Category


class Recipe(BaseModel):
    __table_name__ = "recipes"

    title: str = ""
    level: str = ""
    total: str = ""
    prep: str = ""
    cook: str = ""
    yields: str = ""
    inactive: str = ""
    ingredients: List[str]
    categories: List[str]

    def __init__(self, **attributes):
        self.__dict__.update(attributes)

    def __repr__(self):
        return "<Recipe title=%r>" % self.title

    @classmethod
    def all(self):
        cursor = self.__table__().run(self.__db__().conn)
        return [Recipe(**row) for row in cursor]

    @classmethod
    def find(self, id):
        adict = self.__table__().get(id).run(self.__db__().conn)
        return Recipe(**adict)

    @classmethod
    def destroy(self, id):
        self.__table__().get(id).delete().run(self.__db__().conn)

    def update(self, **attributes):
        for key, value in attributes.items():
            setattr(self, key, value)
        self.__class__.__table__().get(self.id).update(self.to_dict()).run(
            self.__class__.__db__().conn
        )
        return self

    @property
    def ingredients(self):
        return list(map(lambda r: r.name, Ingredients.all(self)))

    def create_ingredients(self, attrs):
        return Ingredients.create(self, *attrs)

    def delete_ingredients(self, name):
        return Ingredients.destroy(self, name)

    def update_ingredients(self, name, position):
        return Ingredients.update(self, name, position)

    @property
    def categories(self):
        return Category.find(
            self.__class__.__table__()
            .get(self.id)["categories"]
            .run(self.__class__.__db__().conn)
        )

    def remove_category(self, category_id):
        response = (
            self.__class__.__table__()
            .get(self.id)
            .update(
                {
                    "categories": self.__class__.__db__()
                    .r.row["categories"]
                    .filter(lambda id: id != category_id)
                }
            )
            .run(self.__class__.__db__().conn)
        )
        return response["replaced"]

    def add_category(self, category_id):
        response = (
            self.__class__.__table__()
            .get(self.id)
            .update(
                {
                    "categories": self.__db__()
                    .r.row["categories"]
                    .default([])
                    .append(category_id)
                },
                return_changes=True,
            )
            .run(self.__class__.__db__().conn)
        )
        if response["replaced"] == 1:
            categories = response["changes"][0]["new_val"]["categories"]
            return Category.find(categories)

    def to_dict(self):
        return dict(
            title=self.title,
            level=self.level,
            total=self.total,
            prep=self.prep,
            cook=self.cook,
            yields=self.yields,
            id=self.id,
            inactive=self.inactive,
            ingredients=self.ingredients,
        )

    @classmethod
    def create(self, **attributes):
        response = (
            self.__table__()
            .insert(
                dict(
                    title=attributes.get("title", ""),
                    level=attributes.get("level", ""),
                    total=attributes.get("total", ""),
                    prep=attributes.get("prep", ""),
                    cook=attributes.get("cook", ""),
                    yields=attributes.get("yields", ""),
                    inactive=attributes.get("inactive", ""),
                )
            )
            .run(self.__db__().conn)
        )

        if len(response["generated_keys"]) > 0:
            return self.find(response["generated_keys"][0])


class Ingredients:
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return "<Ingredients name=%r>" % self.name

    @staticmethod
    def instace(name):
        return Ingredients(name=name)

    @classmethod
    def all(self, recipe):
        __db__ = recipe.__class__.__db__()
        __table__ = recipe.__class__.__table__()
        if __table__.get(recipe.id).has_fields("ingredients").run(__db__.conn):
            result = __table__.get(recipe.id)["ingredients"].run(__db__.conn)
            return list(map(Ingredients.instace, result))
        else:
            return []

    @classmethod
    def update(self, recipe, name, position):
        __db__ = recipe.__class__.__db__()
        __table__ = recipe.__class__.__table__()
        result = (
            __table__.get(recipe.id)
            .update(
                {
                    "ingredients": __db__.r.row["ingredients"].change_at(
                        position, name
                    )
                }
            )
            .run(__db__.conn)
        )

        return recipe.ingredients

    @classmethod
    def destroy(self, recipe, recipe_name):
        __db__ = recipe.__class__.__db__()
        __table__ = recipe.__class__.__table__()
        __table__.get(recipe.id).update(
            {
                "ingredients": __db__.r.row["ingredients"].filter(
                    lambda name: name != recipe_name
                )
            }
        ).run(__db__.conn)

        return recipe.ingredients

    @classmethod
    def create(self, recipe, *attrs):
        __db__ = recipe.__class__.__db__()
        __table__ = recipe.__class__.__table__()

        for attr in attrs:
            __table__.get(recipe.id).update(
                {
                    "ingredients": __db__.r.row["ingredients"]
                    .default([])
                    .append(attr)
                }
            ).run(__db__.conn)

        result = __table__.get(recipe.id)["ingredients"].run(__db__.conn)

        return list(map(Ingredients.instace, result))
