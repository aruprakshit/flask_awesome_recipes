from .base_model import BaseModel
from . import bcrypt
from datetime import date
import secrets
from rethinkdb.errors import ReqlCursorEmpty


class User(BaseModel):
    __table_name__ = "users"

    username: str = ""
    password_hash: str = ""
    issued_at: date
    refresh_token: str
    access_token: str
    expires_in: int
    expires_on: int

    def __init__(self, **attributes):
        self.set_password(attributes.pop("password", None))
        self.__dict__.update(attributes)

    def set_password(self, password):
        if password:
            self.password_hash = bcrypt.generate_password_hash(password)

    @staticmethod
    def generate_token():
        return secrets.token_urlsafe(20)

    def reset_token(self):
        response = (
            self.__class__.__table__()
            .get(self.id)
            .update(
                dict(
                    access_token="",
                    expires_in=0,
                    expires_on=0,
                    refresh_token="",
                    issued_at=None,
                ),
                return_changes=True,
            )
            .run(self.__class__.__db__().conn)
        )
        if response["replaced"] == 1:
            self.__dict__.update(response["changes"][0]["new_val"])

    def set_tokens(self):
        response = (
            self.__class__.__table__()
            .get(self.id)
            .update(
                dict(
                    access_token=User.generate_token(),
                    expires_in=3600,
                    expires_on=1479937454,
                    refresh_token=User.generate_token(),
                    issued_at=self.__class__.__db__().r.now().date(),
                ),
                return_changes=True,
            )
            .run(self.__class__.__db__().conn)
        )
        if response["replaced"] == 1:
            self.__dict__.update(response["changes"][0]["new_val"])

    def check_password(self, password):
        return bcrypt.check_password_hash(self.password_hash, password)

    def to_dict(self):
        return dict(
            username=self.username,
            id=self.id,
            access_token=self.access_token,
            expires_in=self.expires_in,
            expires_on=self.expires_on,
            refresh_token=self.refresh_token,
            issued_at=self.issued_at.date(),
        )

    def __repr__(self):
        return "<User username=%r>" % self.username

    @classmethod
    def destroy(cls, id):
        cls.__table__().get(id).delete().run(cls.__db__().conn)

    @classmethod
    def find(cls, id):
        adict = cls.__table__().get(id).run(cls.__db__().conn)
        return User(**adict)

    @classmethod
    def find_by(cls, key, value):
        query = {}
        query[key] = value
        cursor = cls.__table__().filter(query).limit(1).run(cls.__db__().conn)
        try:
            instance = User(**cursor.next())
            return instance
        except ReqlCursorEmpty:
            return None

    @classmethod
    def create(cls, **attributes):
        instance = cls(**attributes)
        response = (
            cls.__table__()
            .insert(
                dict(
                    username=instance.username,
                    password_hash=instance.password_hash,
                )
            )
            .run(cls.__db__().conn)
        )

        if len(response["generated_keys"]) > 0:
            instance.id = response["generated_keys"][0]
            return instance
