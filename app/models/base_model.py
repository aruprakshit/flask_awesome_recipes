from app.db import get_db

# https://rethinkdb.com/api/python


class BaseModel:
    @classmethod
    def __table__(self):
        return self.__db__().r.table(self.__table_name__)

    @staticmethod
    def __db__():
        return get_db()
