from typing import List

from .base_model import BaseModel


class Category(BaseModel):
    __table_name__ = "categories"

    title: str = ""

    def __init__(self, **attributes):
        self.__dict__.update(attributes)

    def __repr__(self):
        return "<Category title=%r>" % self.title

    @classmethod
    def all(self):
        cursor = self.__table__().run(self.__db__().conn)
        return [Category(**row) for row in cursor]

    def to_dict(self):
        return dict(title=self.title, id=self.id)

    @classmethod
    def update(self, id, **payload):
        response = (
            self.__table__().get(id).update(payload).run(self.__db__().conn)
        )
        if response["replaced"] > 0:
            return self.find(id)

    @classmethod
    def find(self, id):
        if isinstance(id, list):
            rows = self.__table__().get_all(*id).run(self.__db__().conn)
            return [Category(**row) for row in rows]
        else:
            adict = self.__table__().get(id).run(self.__db__().conn)
            return Category(**adict)

    @classmethod
    def destroy(self, id):
        self.__table__().get(id).delete().run(self.__db__().conn)

    @classmethod
    def create(self, **attributes):
        response = (
            self.__table__()
            .insert(dict(title=attributes.get("title", "")))
            .run(self.__db__().conn)
        )

        if len(response["generated_keys"]) > 0:
            return self.find(response["generated_keys"][0])
